
from pygeocoder import Geocoder
import xlrd
import os
from functools import partial
oop_data = "BPO.xls"
data_file = "cor_addres.dat"


def get_addres_by(addres, city='Poznan'):

    def search_for_near(city, addres, my_loc):
        coord = (my_loc.coordinates[0], my_loc.coordinates[1])
        #a little function which calculates the distance between two coordinates
        dist = lambda s, d: (s[0]-d[0])**2+(s[1]-d[1])**2

        with open(data_file) as f:
            content_file = f.read()
            list_with_cordinates = []
            arrary = []
            for line in content_file.split('|'):
                arrary.append(line)

                if len(arrary) == 3:
                    list_with_cordinates.append(arrary[:])
                    arrary = []

        to_compeare_list = []
        for item in list_with_cordinates:
            to_compeare_list.append((float(item[1]), float(item[2])))

        near_place_cord = min(to_compeare_list, key=partial(dist, coord))
        for search_item in list_with_cordinates:

            if (
                    float(search_item[1]) == near_place_cord[0] and
                    float(search_item[2]) == near_place_cord[1]
            ):

                return search_item[0]

    def get_data_by_row(id_of_location, sheet):
        return sheet.row(int(id_of_location))

    myexcel = xlrd.open_workbook(oop_data)
    sheet = myexcel.sheet_by_index(0)

    my_loc = Geocoder.geocode(addres + ' , ' + city)
    print addres + ' , ' + city + ' ' + str(my_loc[0])

    if os.path.exists(data_file):
        id_of_location = search_for_near(city, addres, my_loc)
    else:
        create_file = file(data_file, 'w')

        for row_index in range(1, sheet.nrows):

            get_street = sheet.cell(row_index, 2).value
            object_geo = Geocoder.geocode(get_street + ' , ' + city)

            create_file.write(
                str(row_index + 1) + '|' +
                str(object_geo[0].coordinates[0]) + '|' +
                str(object_geo[0].coordinates[1]) + '|'
            )
        id_of_location = search_for_near(city, addres, my_loc)

    location_data_list = get_data_by_row(id_of_location, sheet)

    return location_data_list

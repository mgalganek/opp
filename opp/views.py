"""File with views (pages) this site"""
from pyramid.threadlocal import get_current_registry
from pyramid.view import view_config
from lib.parase_xls import get_addres_by


class ProjectorViews(object):
    '''Main class with all views'''
    def __init__(self, request):
        self.request = request
        self.project = 'opp'
        self.ini_set = get_current_registry().settings

    @view_config(route_name='home', renderer='opp:templates/home.mako',
                 permission='view')
    def home_page(self):
        return {'project': 'opp'}

    @view_config(renderer="json", name="search.json")
    def comparing(self):
        addres = self.request.params
        addres = str(addres['q'])
        if addres == '':
            return None
        addres = addres.lower()
        addres = addres.strip()

        lists = get_addres_by(addres)
        list2 = []
        for item in lists:
            list2.append(str(item).split(':', 1)[1])

        org_name = list2[1]
        org_addres = list2[2]
        org_www = list2[10]
        org_yes = list2[12]
        return {
            'project': 'opp',
            'product': '',
            'org_name': org_name[2:-1],
            'org_addres': org_addres[2:-1],
            'org_www': org_www[2:-1],
            'org_yes': org_yes[2:-1],
        }

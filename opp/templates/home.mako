## home
<%inherit file="base_html.mako"/>

	<div class="middle align-center">
		<h2>Napisz swój adres, a odnajdziesz najbliższą organizację pozarządową </h2>
		<form id="search_product_form" method="post" action="search.json">
			<input type="text" id="q" name="q" value="" />
			<input type="submit" id="x" value="Szukaj" />
		</form>
	</div>
	<div class="middle align-center">
		<h2><span id="addres"></span></h2>
		<div id="results"></div>
	</div>
	<center><img src="${request.static_url('opp:static/logo.png')}" width="250" height="72" alt="pyramid"/></center>
	<center>
	<center>Autorzy: Mateusz Gałganek, Wojtek Sańko. Stworzone w ramach <a href="http://startup.poznan.pl/hackathon/">Hackathon - Startup Poznań</a></center>



## base_html
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" xmlns:tal="http://xml.zope.org/namespaces/tal">
<head>
  <title>Poznańskie organizacje pozarządowe</title>
  <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
  <meta name="keywords" content="python web application" />
  <meta name="description" content="pyramid web application" />
  <link rel="shortcut icon" href="${request.static_url('opp:static/favicon.ico')}" />
  <link rel="stylesheet" href="${request.static_url('opp:static/pylons.css')}" type="text/css" media="screen" charset="utf-8" />
  <link rel="stylesheet" href="http://static.pylonsproject.org/fonts/nobile/stylesheet.css" media="screen" />
  <link rel="stylesheet" href="http://static.pylonsproject.org/fonts/neuton/stylesheet.css" media="screen" />
 
  <link rel="stylesheet" href="${request.static_url('opp:static/global_layout.css')}" type="text/css" media="screen" charset="utf-8" />
  <script type="text/javascript" src="${request.static_url('opp:static/jquery.js')}"></script>
  <script src="http://malsup.github.com/jquery.form.js"></script>
  <script src="${request.static_url('opp:static/read_dom.js')}"></script>
  <script src="http://maps.google.com/maps/api/js?sensor=false" 
          type="text/javascript"></script>
  <link rel="stylesheet" href="/static/form.css" type="text/css" />
  <script type="text/javascript" src="${request.static_url('opp:static/scripts/deform.js')}"></script>
  <!--[if lte IE 6]>
  <link rel="stylesheet" href="${request.static_url('opp:static/ie6.css')}" type="text/css" media="screen" charset="utf-8" />
  <![endif]-->
</head>
    <body>
		<div class="header">
			<div class="top align-center">
				<div>
					<a href="/">
  <img src="${request.static_url('opp:static/logo3.jpg')}" width="360" height="" alt="pyramid"/>
<img src="${request.static_url('opp:static/logo2.png')}" width="220" height="220" alt="pyramid"/>

				</div>
					</a>
			</div>
		</div>
		<div id="wrap">
		${next.body()}
		</div>
	</body>
	<script type="text/javascript">
   deform.load()
	</script>
</html>

$(document).ready(function() {
	$('#search_product_form').ajaxForm({
		dataType: 'json',
		success: get_updates
	});
	function get_updates(data){
		var text_product = $('#product');
		console.log(text_product);
		text_product.empty();
		text_product.append('Wyniki');
		
		var target = $('#results');
		console.log(target);
		target.empty();
		target.append('<br>');
		target.append('<a href="http://' + data.org_www + '">' + data.org_name + '</a>');
		target.append('<br>');
		target.append('Adres: ');
		target.append('<a href="http://www.google.pl/maps/place/Poznan+' + data.org_addres + '">  ' + data.org_addres + '</a>');
		target.append('<br>');
		target.append('Czy możesz przekazać 1% podatku: ' + data.org_yes);
		target.append('<br>');
		target.append('Czy możesz przekazać darowiznę: TAK');
	}
});

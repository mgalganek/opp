import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.txt')).read()
CHANGES = open(os.path.join(here, 'CHANGES.txt')).read()

requires = [
    'pyramid',
    'pyramid_debugtoolbar',
    'waitress',
    'deform',
    'colander',
    'mechanize',
    'beautifulsoup4',
    'deform',
    'lxml',
    'mako',
    'argparse',
    'pyramid_tm',
    'pygeocoder',
    'xlrd'
]

setup(name='opp',
      version='0.1',
      description='opp',
      long_description=open('README.txt').read(),
      classifiers=[
          "Programming Language :: Python",
          "Framework :: Pyramid",
          "Topic :: Internet :: WWW/HTTP",
          "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
      ],
      author='',
      author_email='galganek.mateusz@gmail.com',
      url='',
      keywords='',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=requires,
      test_suite="tests",
      entry_points="""
      [paste.app_factory]
      main = opp:main
      """,
      )
